import struct

def recv_msg(sock):
    # Читаем длину сообщения
    raw_msglen = recvall(sock, 4)
    if not raw_msglen:
        return None
    msglen = struct.unpack('>I', raw_msglen)[0]
    # Читаем само сообщение
    return recvall(sock, msglen)

def recvall(sock, n):
    data = bytes()
    while len(data) < n:
        packet = sock.recv(n - len(data))
        if not packet:
            return None
        data += packet

    return data
