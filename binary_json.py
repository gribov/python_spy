import json

def unpack(bin_data):
    '''
    Создает из бинарных данных json строку и из нее
    объект
    '''
    try:
        obj = json.loads(bin_data.decode())
    except json.decoder.JSONDecodeError:
        return False
    else:
        return obj

def pack(obj):
    '''
    Создает из объекта json строку и из нее
    строку бинарных данных
    '''
    return bytes(json.dumps(obj),"utf8")