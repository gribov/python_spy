import threading, queue
import os, socket, struct
from time import sleep, strftime
from random import randrange
from config import SERVER_HOST, SERVER_PORT, MES_PACK_SIZE
import binary_json

def getMessage(some_number):
    '''Возвращает словарь с данными'''

    message = 'foo bar (%d)' % some_number
    if not message : return False

    return {
        'win_id' : username,
        'date_time' : strftime('%Y-%m-%d %H:%M:%S'),
        'message' : message,
    }

def myLogging(m_queue):
    # Функция логирования. Запускается как поток-демон
    current_window = True
    while True:
        some_number = randrange(1, 10)
        sleep(1)
        if(some_number >= 5):
            log = getMessage(some_number)
            if log : m_queue.put(log)

def messageQueueMonitoring(m_queue):
    # Функция которая следит за состоянием очереди данных
    # Запускается как поток-демон
    # Если очередь полная, то сообщения извлекаются из очереди
    # И происходит их отправка
    while True:
        if m_queue.full():
            messages = getAllQueueMessages(m_queue) # берем все сообщения из очереди
            sendToServer(messages) # отправляем на сервер статистики

def getAllQueueMessages(m_queue):

    # Очищает очередь извлекая все сообщения
    # И возвращает данные сообщения как список

    messages = []
    while True:
        try :
            messages.append(m_queue.get_nowait())
            m_queue.task_done()
        except  queue.Empty:
            break
    return messages

def sendToServer(logMessages):

    # Принимает данные в виде списка сообщений
    # Каждый элемент списка - словарь
    # Создаем соединение с сокетом сервера
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        try:
            sock.connect((SERVER_HOST, SERVER_PORT))
        except ConnectionRefusedError as e:
            # Логируем ошибку и ответ от сервера
            print('Server connection error.[Error code : %d]' % e.errno)
            #servicemanager.LogErrorMsg()
        else:
            pack = binary_json.pack(logMessages)
            # Упаковываем к каждому пакету его длину в первые 4 байта
            pack = struct.pack('>I', len(pack)) + pack
            sock.sendall(pack)
            try :
                response = binary_json.unpack(sock.recv(1024))
            except ConnectionResetError as e:
                print('Server connection refused error.[Error code : %d]' % e.errno)
                response = False

            if response and response['status'] == 1: # ответ от сервера ОК
                print('Successful send logs to server.[%d log massages sent]' %
                                          response['count_of_inserted_rows'])
                pass
            else : # произошла ошибка данных
                # Логируем ошибку и ответ от сервера
                if type(response) == 'dict':
                    print('Send error.[Server returns : %s]' % response['error_type'])
                else:
                    print('Send error.')


def clientStart():
    ''' Функция запускает клиента '''
    logQueue = queue.Queue(maxsize=MES_PACK_SIZE) # Очередь сообщений к отправке для процесса отправки
                                      # Очередь оторую пополняет процесс логирования действий

    # Процесс логирования действий пользователя запускаем как демон,
    # но главный процесс не закончится
    # т.к. внизу по коду есть цикл
    activityLoggerThread = threading.Thread(name='user-activity-daemon', target=myLogging, args=(logQueue,))
    activityLoggerThread.setDaemon(True)
    activityLoggerThread.start()

    # Процесс слежения за очередью
    senderThread = threading.Thread(name='send-activity-daemon', target=messageQueueMonitoring, args=(logQueue,))
    senderThread.setDaemon(True)
    senderThread.start()

    while True : pass # Бесконечный цикл

if __name__ == '__main__':
    username = os.getlogin() # текущий пользователь
    clientStart()
