import glob
import os
import sys
import shutil
from time import sleep
from datetime import datetime
import logging
import argparse
from formatter import ColorLogFormatter


def logResults(logger, counter):
    logger.info(
        'Синхронизация завершена.Cоздано: %d, обновлено: %d, удалено: %d' %
        (counter['created'], counter['updated'], counter['deleted'])
    )


def getDirList(path):
    return [f for f in glob.glob(path + "/**/*", recursive=True)]


def clearReplicaFromOldFiles(rootSourcePath, rootReplicaPath):
    counter = 0
    files = getDirList(rootReplicaPath)

    for replicaPath in files:
        sourcePath = replicaPath.replace(rootReplicaPath, rootSourcePath)
        if not os.path.exists(sourcePath):
            # в реплике остался файл или директория, которой нет в источнике. Удаляем
            logger.debug('Удаление %s' % replicaPath)
            if os.path.isdir(replicaPath):
                shutil.rmtree(replicaPath)
            elif os.path.isfile(replicaPath):
                os.remove(replicaPath)
            else:
                logger.debug('Файл или директория уже удалены %s.' % replicaPath)
            counter += 1

    return counter


def startSync(rootSourcePath, rootReplicaPath, logger):
    logger.info('Запуск синхронизации')
    counter = {
        'created': 0,
        'updated': 0,
        'deleted': 0
    }
    counter['deleted'] = clearReplicaFromOldFiles(rootSourcePath, rootReplicaPath)

    if not os.path.exists(rootReplicaPath):
        logger.debug('Создание директории %s' % sourceDirPath)
        # os.mkdir(rootReplicaPath)
        # просто копируем все дерево целиком, но тогда надо залогировать создание всех файлов
        shutil.copytree(rootSourcePath, rootReplicaPath)
        files = getDirList(rootSourcePath)
        counter['created'] = len(files)
        for f in files:
            replicaPath = f.replace(rootSourcePath, rootReplicaPath)
            logger.debug('Копирование %s в %s' % (f, replicaPath))
        logResults(logger, counter)

        return

    files = getDirList(rootSourcePath)

    for sourcePath in files:
        sourceFileChangeTime = os.path.getmtime(sourcePath)
        sourceFileChangeTimeString = datetime.fromtimestamp(
            sourceFileChangeTime).strftime('%Y-%m-%d %H:%M:%S'
                                           )
        replicaPath = sourcePath.replace(rootSourcePath, rootReplicaPath)
        isFile = os.path.isfile(sourcePath)
        isDir = os.path.isdir(sourcePath)

        # в источнике это директория и ее нет в копии
        if os.path.isdir(sourcePath):
            if not os.path.isdir(replicaPath):
                logger.debug('Создание директории %s' % replicaPath)
                os.mkdir(replicaPath)
                counter['created'] += 1
            continue

        if isDir and not os.path.isdir(replicaPath):
            logger.debug('Создание директории %s' % replicaPath)
            # новый файл лежит в директории которой еще нет в реплике, создаем ее
            os.makedirs(replicaPath)

        elif isFile:
            # в реплике уже есть этот файл
            if os.path.isfile(replicaPath):
                replicaFileChangeTime = os.path.getmtime(replicaPath)
                replicaFileChangeTimeString = datetime.fromtimestamp(
                    replicaFileChangeTime).strftime('%Y-%m-%d %H:%M:%S')
                # в реплике старый файл
                if replicaFileChangeTime != sourceFileChangeTime:
                    logger.debug('Копирование файла с заменой %s (от %s) на %s (от %s)' % (
                        sourcePath, sourceFileChangeTimeString, replicaPath, replicaFileChangeTimeString))

                    os.remove(replicaPath)
                    shutil.copy2(
                        os.path.abspath(sourcePath),
                        os.path.abspath(replicaPath)
                    )
                    counter['updated'] += 1
            else:
                # нет этого файла
                logger.debug('Копирование файла %s в %s' %
                            (sourcePath, replicaPath))
                # Проверяем, есть ли директория
                if not os.path.isdir(os.path.dirname(replicaPath)):
                    logger.debug('Создание директории %s' %
                                os.path.dirname(replicaPath))
                    # новый файл лежит в директории которой еще нет в реплике, создаем ее
                    os.makedirs(os.path.dirname(replicaPath))

                shutil.copy2(
                    os.path.abspath(sourcePath),
                    os.path.abspath(replicaPath)
                )
                counter['updated'] += 1

    logResults(logger, counter)


def createLogger(logFile):
    fmt = '%(asctime)s %(message)s'
    datefmt = '%Y-%m-%d %H:%M:%S'
    logger = logging.getLogger('replicator')
    logger.setLevel(logging.DEBUG)

    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setLevel(logging.DEBUG)
    colorFormatter = ColorLogFormatter(fmt=fmt, datefmt=datefmt)
    stdout_handler.setFormatter(colorFormatter)
    logger.addHandler(stdout_handler)


    if logFile:
        fileLogFormatter = logging.Formatter(fmt=fmt, datefmt=datefmt)
        logFilePath = os.path.join(cwd, os.path.normpath(logFile))
        fh = logging.FileHandler(logFilePath)
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(fileLogFormatter)
        logger.addHandler(fh)

    return logger


def getArgs():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('sourceDir', type=str, help='path to sourceDir')
    parser.add_argument('replicaDir', type=str, help='path to replicaDir')
    parser.add_argument('-p', '--period', type=int,
                        help='period in seconds', default=0)
    parser.add_argument('-f', '--logFile', type=str, help='path to logFile')

    args = parser.parse_args()

    return {
        'sourceDir': args.sourceDir,
        'replicaDir': args.replicaDir,
        'period': args.period,
        'logFile': args.logFile,
    }


if __name__ == "__main__":
    args = getArgs()
    cwd = os.getcwd()

    sourceDirPath = os.path.join(cwd, os.path.normpath(args['sourceDir']))
    replicaDirPath = os.path.join(cwd, os.path.normpath(args['replicaDir']))

    logger = createLogger(args['logFile'])

    if not os.path.isdir(sourceDirPath):
        logger.error('Ошибка: директория-источник не существует')
        quit()

    if args['period']:
        logger.info('Запуск в режиме периодического запуска каждые %d секунд' % args['period'])
        while True:
            startSync(sourceDirPath, replicaDirPath, logger)
            sleep(args['period'])

    startSync(sourceDirPath, replicaDirPath, logger)
