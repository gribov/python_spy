import socket, uuid, logging, sys
import threading
from config import SERVER_HOST, SERVER_KEY_PORT, SERVER_DATA_PORT
import binary

BUFFER_SIZE = 2048
logging.basicConfig(stream=sys.stdout, encoding='utf-8', level=logging.DEBUG)


def sendData(client_id, data, serverPort):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        try:
            logging.debug('Try to connect server .[clientId : %s]' % client_id)
            sock.connect((SERVER_HOST, serverPort))
        except ConnectionRefusedError as e:
            # Логируем ошибку и ответ от сервера
            logging.error('Server connection error.[Error key : %d]' % e.errno)
        else:
            try:
                logging.debug('Successful connection to server .[clientId : %s]' % client_id)
                sock.sendall(binary.pack(data))
                response = binary.unpack(sock.recv(BUFFER_SIZE))

                if response and response['status'] == 1:  # ответ от сервера ОК
                    logging.info('Successful response from server %s' % response)
                    return response
                else:  # произошла ошибка
                    logging.error('Request data error.[Server returns: %s]' % response['error'])
                    return False

            except socket.error as e:
                logging.error('Server data request error.[Error key : %d]' % e.errno)
            finally:
                sock.close()

def requestKeyFromServer(client_id):
    data = {'client_id': client_id}
    return sendData(client_id, data, SERVER_KEY_PORT)


def sendDataToServer(client_id, key):
    data = {
        'client_id': client_id,
        'key': key,
        'data': 'hello server, how are you?'
    }

    return sendData(client_id, data, SERVER_DATA_PORT)


def makeClientRequest(id):
    client_id = 'client_%d_%s' % (id, uuid.uuid4().hex.upper())
    response = requestKeyFromServer(client_id)
    if response:
        key = response['key']
        if not key:
            logging.error('Code request error')
            quit('Code request error')
        sendDataToServer(client_id, key)
    else:
        logging.error('Key request error')

if __name__ == '__main__':
    # makeClientRequest(1)

    threads = list()
    for index in range(10):
        logging.info("Main: create and start thread %d.", index)
        tr = threading.Thread(target=makeClientRequest, args=(index,))
        threads.append(tr)
        tr.start()
        tr.join()
