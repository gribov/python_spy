import uuid
import logging, threading, sys
from socketserver import ThreadingTCPServer, BaseRequestHandler
import binary
import recv_tools
from config import SERVER_HOST, SERVER_KEY_PORT, SERVER_DATA_PORT

keysStore = {}

logging.basicConfig(stream=sys.stdout, encoding='utf-8', level=logging.DEBUG)

def writeToLog(client_id, message):
    fname = 'log.txt'
    with open(fname, 'a+') as f:
        f.write('[clientId:%s]%s\n' % (client_id, message))

class TCPKeyRequestHandler(BaseRequestHandler):
    def handle(self):
        binaryFromRequest = recv_tools.recv_msg(self.request)
        self.data = binary.unpack(binaryFromRequest) # из бин.данных создаем объект
        if not self.data:
            response = {
                'status' : 0,
                'error' : 'Data structure error' # Ошибка данных
                }
        elif self.data['client_id']:
            key = 'KEY-' + uuid.uuid4().hex.upper()
            keysStore[self.data['client_id']] = key

            response = {
                'status' : 1,
                'key' : key
                }

        self.request.sendall(binary.pack(response))

def isKeyValid(client_id, key):
    isValid = client_id in keysStore and keysStore[client_id] == key
    if isValid:
        del keysStore[client_id]
    return isValid

class TCPDataRequestHandler(BaseRequestHandler):
    def handle(self):
        binaryFromRequest = recv_tools.recv_msg(self.request)
        self.data = binary.unpack(binaryFromRequest) # из бин.данных создаем объект
        hasKeyAndClientId = self.data['client_id'] and self.data['key']

        if not hasKeyAndClientId:
            response = {
                'status' : 0,
                'error' : 'key and client_id are required'
                }

        if not isKeyValid(self.data['client_id'], self.data['key']):
            response = {
                'status' : 0,
                'error' : 'key is invalid'
                }
        else:
            logging.info(self.data)

            data = self.data['data']
            if data:
                writeToLog(self.data['client_id'], data)

            response = {
                'status' : 1,
                'error' : False
                }

        self.request.sendall(binary.pack(response))


if __name__ == "__main__":
    server1 = ThreadingTCPServer((SERVER_HOST, SERVER_KEY_PORT), TCPKeyRequestHandler)
    server2 = ThreadingTCPServer((SERVER_HOST, SERVER_DATA_PORT), TCPDataRequestHandler)

    tread = threading.Thread(name='request-key-handler-daemon', target=server1.serve_forever)
    tread.setDaemon(True)
    tread.start()

    server2.serve_forever()
