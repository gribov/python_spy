import struct

def recv_msg(sock):
    packet = sock.recv(1024)
    if not packet:
        return None

    return packet

