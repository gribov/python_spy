import recv_tools
import database
import socketserver
import binary_json
from config import SERVER_HOST, SERVER_PORT

class MyTCPHandler(socketserver.BaseRequestHandler):
    """
    Класс сокет-сервера.
    Для информации ссылка : https://docs.python.org/3.5/library/socketserver.html
    """

    def handle(self):
        """
        Перехватывает запросы к порту сервера
        Запускает обработку
        Предполагается, что будет прислан список, каждый элемент которого словарь
        """
        # self.request это TCP socket
        binaryFromRequest = recv_tools.recv_msg(self.request)
        self.data = binary_json.unpack(binaryFromRequest) # из бин.данных создаем объект
        if not self.data:
            response = {
                'status' : 0,
                'error_type' : 'Data structure error' # Ошибка данных
                }
        else:
            database.log(self.data, from_ip=self.client_address[0])

            response = {
                'status' : 1,
                'count_of_inserted_rows' : len(self.data)
                }

        self.request.sendall(binary_json.pack(response))


if __name__ == "__main__":
    # Сервер слушает порт. Обрабатывает сообщения
    server = socketserver.TCPServer((SERVER_HOST, SERVER_PORT), MyTCPHandler)

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()
