import json

def makeObjectFromBinaryData(bin_data):
    '''
    Создает из бинарных данных json строку и из нее
    объект
    '''
    try:
        obj = json.loads(bin_data.decode("utf8"))
    except json.decoder.JSONDecodeError:
        return False
    else:
        return obj

def makeBinaryDataFromObject(obj):
    '''
    Создает из объекта json строку и из нее
    строку бинарных данных
    '''
    return bytes(json.dumps(obj),"utf8")
